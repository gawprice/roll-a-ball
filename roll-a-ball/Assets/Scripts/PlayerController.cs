using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour {
	
	public float speed;
	public TextMeshProUGUI countText;
	public TextMeshProUGUI keyText;
	public GameObject winTextObject; 
	public GameObject loseTextObject;

    private float movementX;
    private float movementY;

	private Rigidbody rb;
	private int count;
	private int keys;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		count = 100;
		SetCountText ();
                loseTextObject.SetActive(false);
				winTextObject.SetActive(false);
		keys = 0;
		SetKeyText ();
	}

	void FixedUpdate ()
	{
		Vector3 movement = new Vector3 (movementX, 0.0f, movementY);
		rb.AddForce (movement * speed);
	}
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ("PickUp"))
		{
			other.gameObject.SetActive (false);
			count = count + 50;
			SetCountText ();
		}
		if (other.gameObject.CompareTag ("Anti"))
		{
			other.gameObject.SetActive (false);
			count = count - 50;
			SetCountText ();
		}
		if (other.gameObject.CompareTag ("Key"))
		{
			other.gameObject.SetActive (false);
			keys = keys + 1;
			SetKeyText ();
		}
		if (other.gameObject.CompareTag ("Door"))
		{
			if (keys > 0)	
			{	other.gameObject.SetActive (false);
				keys = keys - 1;
				SetKeyText ();
			}
		}
		if (other.gameObject.CompareTag ("Win"))
		{
			winTextObject.SetActive(true);
		}
	}
    void OnMove(InputValue value)
    {
        Vector2 v = value.Get<Vector2>();
        movementX = v.x;
        movementY = v.y;
    }
    void SetCountText()
	{
	countText.text = "Health:  " + count.ToString();
	if (count <= 0) 
	{
        loseTextObject.SetActive(true);
	}
	}
	void SetKeyText()
	{
	keyText.text = "Keys: " + keys.ToString();
	}
}